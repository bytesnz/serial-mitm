export function createSpy(options: Options): {
    port1: SerialPort<import("@serialport/bindings-cpp").AutoDetectTypes>;
    port2: SerialPort<import("@serialport/bindings-cpp").AutoDetectTypes>;
    /** @type {HandlerChange}
     * Add an event handler
     */
    on: HandlerChange;
    /** @type {HandlerChange}
     * Remove an event handler
     */
    off: HandlerChange;
    /**
     * Close the serial ports
     */
    close: () => void;
};
export function formatData(data: Iterable<number>, hex: boolean): string;
export function formatChar(c: number, hex: boolean): string;
export type Options = {
    /**
     * Port 1
     */
    port1: string;
    /**
     * Port 2
     */
    port2: string;
    /**
     * Port 1 baudrate
     */
    baud1: number;
    /**
     * Port 2 baudrate
     */
    baud2: number;
    /**
     * If false, won't forward port1 data
     * to port2 (default: true)
     */
    forwardPort1Data: boolean;
    /**
     * If false, won't forward port2 data
     * to port1 (default: true)
     */
    forwardPort2Data: boolean;
};
export type PortDataEventHandlerChange = (event: 'port1Data' | 'port2Data', handler: (data: Buffer) => void) => any;
export type DataEventHandlerChange = (event: 'data', handler: (data: Buffer, port: 1 | 2) => void) => any;
export type PortErrorEventHandlerChange = (event: 'port1Error' | 'port2Error', handler: (error: Buffer) => void) => any;
export type ErrorEventHandlerChange = (event: 'error', handler: (error: Buffer, port: 1 | 2) => void) => any;
export type HandlerChange = PortDataEventHandlerChange | DataEventHandlerChange | PortErrorEventHandlerChange | ErrorEventHandlerChange;
import { SerialPort } from "serialport/dist/serialport";
