#!/bin/env node

import { SerialPort } from 'serialport';
import { exit, argv, argv0, stderr, stdin, stdout } from 'node:process';
import { createInterface } from 'node:readline';
import chalk from 'chalk';
import minimist from 'minimist';

const args = minimist(argv.slice(2));

const rl = createInterface({ input: stdin, output: stdout });

const ports = await SerialPort.list();

console.log('Serial Spy');

const help = () => {
	console.log(`${process.argv0}

Person-in-the-middle serial communications. Requires virtual com ports, like
those created by [com0com](http://com0com.sourceforge.net/) Windows or
socat on Linux.

Usage:
${process.argv0} [-h] [-1 <port1>[,<baudrate1>[,<label1>]] [-2 <port1>[,<baudrate2>[,<label2>]]]

  -1 <port1>[,<baudrate1>[,<label1>]]   The first port (and optionally baudrate
      and label) to person-in-the-middle
  -2 <port2>[,<baudrate2>[,<label2>]]   The second port (and optionally
      baudrate and label) to person-in-the-middle
  -H,--hex                   Display all data as hex
  -h,--help                  Show this help
`);
};

if (args.h || args.help) {
	help();
	exit(0);
}

const hex = args.H || args.hex ? true : false;

const question = (q) =>
	new Promise((resolve, reject) => {
		rl.question(q, resolve);
	});

// Check if have ports
let port1;
let baud1;
let port1Label;
let port2;
let baud2;
let port2Label;

let current = null;
let timeout = null;

if (args[1]) {
	if (args[1].indexOf(',') !== -1) {
		[port1, baud1, port1Label] = args[1].split(',');
		baud1 = Number(baud1);
		if (isNaN(baud1)) {
			stderr.write('Invalid baudrate for port 1 given\n');
			help();
			exit(1);
		}
		if (!port1Label) {
			port1Label = port1;
		}
	} else {
		port1 = args[1];
		port1Label = port1;
	}
}

if (args[2]) {
	if (args[2].indexOf(',') !== -1) {
		[port2, baud2, port2Label] = args[2].split(',');
		baud2 = Number(baud2);
		if (isNaN(baud2)) {
			stderr.write('Invalid baudrate for port 2 given\n');
			help();
			exit(1);
		}
		if (!port2Label) {
			port2Label = port2;
		}
	} else {
		port2 = args[2];
		port2Label = port2;
	}
}

if (!port1 || !port2) {
	stdout.write('Available serial ports:\n');
	ports.forEach((port, index) => {
		stdout.write(`${index + 1}: ${port.path}\n`);
	});

	let firstIndex = null;
	let secondIndex = null;
	let answer;

	if (!port1) {
		while (
			firstIndex === null &&
			(answer = Number(await question(`Please select the first serial port (1-${ports.length}): `)))
		) {
			if (!isNaN(answer) && answer > 0 && answer <= ports.length) {
				firstIndex = answer - 1;
			}
		}

		port1 = ports[firstIndex].path;
	}

	if (!port2) {
		while (
			secondIndex === null &&
			(answer = Number(
				await question(`Please select the second serial port (1-${ports.length}): `)
			))
		) {
			if (!isNaN(answer) && answer > 0 && answer <= ports.length) {
				secondIndex = answer - 1;
			}
		}

		port2 = ports[secondIndex].path;
	}
}

const commonBaudrates = [9600, 19200, 57600, 115200];

if (!baud1 || !baud2) {
	stdout.write('Common serial baudrates:\n');
	commonBaudrates.forEach((rate, index) => {
		stdout.write(`${index + 1}: ${rate}\n`);
	});

	let answer;

	while (
		!baud1 &&
		(answer = Number(
			await question(
				`Please select the baudrate for the first port or enter one (1-${commonBaudrates.length}): `
			)
		))
	) {
		if (!isNaN(answer)) {
			if (answer > 0 && answer <= commonBaudrates.length) {
				baud1 = commonBaudrates[answer - 1];
			} else if (answer > 1000) {
				baud1 = Number(answer);
			}
		}
	}

	while (
		!baud2 &&
		(answer = Number(
			await question(
				`Please select the baudrate for the second port or enter one (1-${commonBaudrates.length}): `
			)
		))
	) {
		if (!isNaN(answer)) {
			if (answer > 0 && answer <= commonBaudrates.length) {
				baud2 = commonBaudrates[answer - 1];
			} else if (answer > 1000) {
				baud2 = Number(answer);
			}
		}
	}
}

console.log('Opening serial ports');

const firstPort = new SerialPort({ path: port1, baudRate: baud1 });
const secondPort = new SerialPort({ path: port2, baudRate: baud2 });

const bcdDatetime = () => {
	const date = new Date();

	return new Buffer.from([
		toBcd(Math.floor(date.getFullYear() / 100)),
		toBcd(date.getFullYear() % 100),
		toBcd(date.getMonth() + 1),
		toBcd(date.getDate()),
		toBcd(date.getHour()),
		toBcd(date.getMinute()),
		toBcd(date.getSecond()),
		toBcd(Math.floor(date.getMilliseconds() / 10))
	]);
};

const toBcd = (number) => {
	return 16 * Math.floor(number / 10) + (number % 10);
};

const printChar = (c) => {
	if (!hex && c >= 32 && c <= 126) {
		stdout.write(String.fromCharCode(c));
	} else if (!hex && c == 0x0d) {
		stdout.write(chalk.grey('\\r'));
	} else if (!hex && c == 0x0a) {
		stdout.write(chalk.grey('\\n'));
	} else {
		const hex = c.toString(16);
		stdout.write(chalk.red((hex.length === 1 ? '0' : '') + hex));
	}
};

firstPort.on('error', (error) => {
	stderr.write(`Error on serial port 1: ${error.message}\n`);
	exit(2);
});

secondPort.on('error', (error) => {
	stderr.write(`Error on serial port 2: ${error.message}\n`);
	exit(2);
});

firstPort.on('data', (data) => {
	if (current !== 'first') {
		if (current) {
			stdout.write('\n');
		}
		current = 'first';
		if (timeout) {
			clearTimeout(timeout);
		}
		timeout = setTimeout(() => {
			current = null;
			timeout = null;
			stdout.write('\n');
		}, 1000);
		stdout.write(`${new Date().toISOString()}:${port1Label}:`);
	}
	for (let i = 0; i < data.length; i++) {
		printChar(data[i]);
	}
	secondPort.write(data);
});

secondPort.on('data', (data) => {
	if (current !== 'second') {
		if (current) {
			stdout.write('\n');
		}
		current = 'second';
		if (timeout) {
			clearTimeout(timeout);
		}
		timeout = setTimeout(() => {
			current = null;
			timeout = null;
			stdout.write('\n');
		}, 1000);
		stdout.write(`${new Date().toISOString()}:${port2Label}:`);
	}
	for (let i = 0; i < data.length; i++) {
		printChar(data[i]);
	}
	stdout.write('\n');
	firstPort.write(data);
});
