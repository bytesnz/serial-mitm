# serial-mitm 0.2.0

Machine-in-the-middle (MITM) serial port traffic

[![pipeline status](https://gitlab.com/bytesnz/serial-mitm/badges/main/pipeline.svg)](git+https://gitlab.com/bytesnz/serial-mitm.git/commits/main)
[![serial-mitm on NPM](https://bytes.nz/b/serial-mitm/npm)](https://npmjs.com/package/serial-mitm)
[![license](https://bytes.nz/b/serial-mitm/custom?color=yellow&name=license&value=AGPL-3.0)](git+https://gitlab.com/bytesnz/serial-mitm.git/blob/main/LICENSE)
[![developtment time](https://bytes.nz/b/serial-mitm/custom?color=yellowgreen&name=development+time&value=~3+hours)](git+https://gitlab.com/bytesnz/serial-mitm.git/blob/main/.tickings)
[![contributor covenant](https://bytes.nz/b/serial-mitm/custom?color=purple&name=contributor+covenant&value=v1.4.1+adopted)](git+https://gitlab.com/bytesnz/serial-mitm.git/blob/main/CODE_OF_CONDUCT.md)
[![support development](https://bytes.nz/b/serial-mitm/custom?color=brightgreen&name=support+development&value=$$)](https://liberapay.com/MeldCE)

Person-in-the-middle serial communications. Requires virtual com ports, like
those created by [com0com](http://com0com.sourceforge.net/) Windows or
socat on Linux.

# Usage

# Node Module

```js
import { createSpy } from 'serial-spy';

const spy = createSpy({
	port1: '/dev/tty0',
	baudrate1: 19200,
	port2: '/dev/tty1',
	baudrate2: 19200
});

spy.on('error', (error, port) => console.error('Error on port', port, error));

spy.on('data', (data, port) => console.log('Data on port', port, data));
```

# Command Line Interface

```
Usage:
serial-spy [-h] [-1 <port1>[,<baudrate1>] [-2 <port1>[,<baudrate2>]]

  -1 <port1>[,<baudrate1>]   The first port (and optionally baudrate) to
      person-in-the-middle
  -2 <port2>[,<baudrate2>]   The second port (and optionally baudrate) to
      person-in-the-middle
  -H,--hex                   Display all data as hex
  -h,--help                  Show this help
```

## Development

Feel free to post errors or feature requests to the project
[issue tracker](https://gitlab.com/bytesnz/serial-mitm/issues) or
[email](mailto:contact-project+bytesnz-serial-mitm-36195618-issue-@incoming.gitlab.com) them to us.
**Please submit security concerns as a
[confidential issue](https://gitlab.com/bytesnz/serial-mitm/issues?issue[confidential]=true)**

The source is hosted on [Gitlab](git+https://gitlab.com/bytesnz/serial-mitm.git) and uses
[eslint][], [prettier][], [lint-staged][] and [husky][] to keep things pretty.
As such, when you first [clone][git-clone] the repository, as well as
installing the npm dependencies, you will also need to install [husky][].

```bash
# Install NPM dependencies
npm install
# Set up husky Git hooks stored in .husky
npx husky install
```

# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [0.2.0] - 2022-06-30

### Fixed

- Fixed errors due to undefined variables in `serial-mitm` command

### Added

- Port labels to `serial-mitm` command by adding third optional port argument
  e.g. `-1 /dev/tty0,9600,Device1`. The labels will be used in the logs output
  by `serial-mitm`

## [0.1.0] - 2022-06-15

Initial version!

[0.2.0]: https://gitlab.com/bytesnz/serial-mitm/-/compare/v0.1.0...v0.2.0
[0.1.0]: https://gitlab.com/bytesnz/serial-mitm/-/tree/v0.1.0
[husky]: https://typicode.github.io/husky
[eslint]: https://eslint.org/
[git-clone]: https://www.git-scm.com/docs/git-clone
[prettier]: https://prettier.io/
[lint-staged]: https://github.com/okonet/lint-staged#readme
[gitlab-ci]: https://docs.gitlab.com/ee/ci/
